#!/bin/bash
cd /data/data
rm -rf /data/data/OCR-D-*
rm mets.xml
rm ocrd.log
ocrd-import /data/data

ocrd process 'cis-ocropy-binarize -I OCR-D-IMG -O OCR-D-SEG-PAGE' \
  'tesserocr-segment-region -I OCR-D-SEG-PAGE -O OCR-D-SEG-BLOCK' \
  'tesserocr-segment-line -I OCR-D-SEG-BLOCK -O OCR-D-SEG-LINE'

export TESSDATA_PREFIX=/data/model
ocrd process 'tesserocr-recognize -I OCR-D-SEG-LINE -O OCR-D-OCR-TESSEROCR -P model frk'
